#!/usr/bin/env python
# vim:set ts=4 sw=4 sts=4 et: #

# A REPL/Compiler for (syntactic) Python to ARM Thumb in ~500 LOC.
# Supports:
# - Integer literals
# - Basic array references
# - Arithmetic and comparison ops
# - If/elif/else
# - While
# - Function definitions (4 args max) / calls
# - Local vars
# - Calls to native C functions (4 args max)
#
# Read this file backwards (frontend -> IR -> backend)
#
# References ARM v7-M Architecture Reference Manual (ARM DDI 0403C)

import subprocess
import struct
import array
import code as icode
import ast
import sys
import readline
import atexit
try:
    readline.read_history_file(".replhist")
    readline.set_history_length(1000)
except IOError:
    pass
atexit.register(readline.write_history_file, ".replhist")

# mapping of Python AST ops to IR names, as well as reverse comparison mapping
OPS = {
    ast.Mult: "mul", ast.Div: "div", ast.Add: "add", ast.Sub: "sub",
    ast.Mod: "mod", ast.Eq: "eq", ast.NotEq: "ne", ast.Gt: "gt",
    ast.Lt: "lt", ast.GtE: "ge", ast.LtE: "le", ast.LShift: "shl",
    ast.RShift: "shr", ast.BitAnd: "and", ast.BitOr: "or"
}
REVOP = {ast.Eq: ast.NotEq, ast.NotEq: ast.Eq, ast.GtE: ast.Lt,
         ast.Lt: ast.GtE, ast.Gt: ast.LtE, ast.LtE: ast.Gt}

# ARM conditional execution codes
CCODES = "eq ne cs cc mi pl vs vc hi ls ge lt gt le all".split()

class INS(object):  # Intermediate Representation instructions
    (MOV, LDSP, STRSP, SP, IMM, BIN, BINI, LDR, STR, JMP, CALL, RET) = range(12)
    names = "mov ldsp strsp sp imm bin bini ldr str jmp call ret".split()

# IR/Optimiser/Backend
# We use the worst register allocator ever (if you can even call it that). Use an
# accumulator 'RA' as the destination for all operations (stored in R4), and a second
# register 'RB' for the RHS of ops (stored in R5). A third register 'RC' is used for
# memory store ops (str RC, [RA, RB]). Temps are stored in R6 and R7 then spilled
# to the stack. Arguments are dropped into the stack on function prolog and variables
# + temp spills are stored following them.

RA, RB, RC, T0 = (4, 5, 3, 6)

# Extract fields from a value. Format (width0, ofsset0), .. (widthN, osfsetN)
def bit_extract(v, *fields):
    return [(v >> ofs) & ((1 << width)-1) for width, ofs in fields]

# Combined IR and codegen
class Codegen(object):
    def __init__(self, base, addrs, args):
        self.base, self.addrs, self.args = base, addrs, args
        self.insts = []
        self.code = []
        self.rets = []
        self.env = {}
        self.ntmp = -1
        self.code16(0xB5F8)  # push r3-r7,lr
        self.code16(0xB080)  # sub sp, 0
        for i, arg in enumerate(args):  # save all our arguments to the stack
            self.inst(INS.STRSP, i, self._getvar(arg))

    # ARM Thumb backend

    def code16(self, v):
        self.code.append(v)

    def code32(self, v):
        self.code16(v >> 16)
        self.code16(v & 0xFFFF)

    def _binop_imm(self, op, rd, imm):
        if op == "add": self.code16(0x1C00|rd|(rd<<3)|(imm<<6))
        elif op == "sub": self.code16(0x1E00|rd|(rd<<3)|(imm<<6))
        elif op == "cmp": self.code16(0x2800|(rd<<8)|(imm))
        else: fail(op)

    def _mod(self, rd, rr):
        # m = x % y  --> z = x / y ; m = x - (y * z)
        self.code32(0xFBB0F0F0|(rd<<16)|(8<<8)|rr) # A6.7.145 (T1) UDIV
        self.code32(0xFB000010|(rr<<16)|(rd<<12)|(rd<<8)|8) # A6.7.74 (T1) MLS

    def _binop(self, op, rd, rr):
        if op == "add": self.code16(0x4400|(rr<<3)|(rd)) # A6.7.4 (T2) ADD (reg)
        elif op == "sub": self.code16(0x1A00|(rr<<6)|(rd<<3)|(rd)) # A6.7.133 (T1) SUB (reg)
        elif op == "mul": self.code16(0x4340|(rr<<3)|(rd)) # A6.7.83 (T1) MUL (reg)
        elif op == "div": self.code32(0xFB90F0F0|(rd<<16)|(rd<<8)|(rr)) # A6.7.111 (T1) SDIV
        elif op == "shl": self.code16(0x4080|(rr<<3)|rd) # A6.7.68 (T1) LSL (reg)
        elif op == "shr": self.code16(0x4100|(rr<<3)|rd) # A6.7.11 (T1) ASR (reg)
        elif op == "and": self.code16(0x4000|(rr<<3)|rd) # A6.7.9 (T1) AND (reg)
        elif op == "or":  self.code16(0x4300|(rr<<3)|rd) # A6.7.91 (T1) ORR (reg)
        elif op == "mod": self._mod(rd, rr)
        elif op == "cmp": self.code16(0x4280|(rr<<3)|(rd)) # A6.7.28 (T1) CMP (reg)
        else: fail(op)

    def _make_jump(self, target_addr, addr, t3=False):
        ofs = target_addr - addr - 4
        ofs = ofs if ofs >= 0 else (-ofs-1) ^ 0xFFFFFFFF
        if t3:  # build 20/24bit ARM jump
            imm11, imm10, i2, i1, s = bit_extract(ofs, (11,1), (6,12), (1,18), (1,19), (1,20))
        else:
            imm11, imm10, i2, i1, s = bit_extract(ofs, (11,1), (10,12), (1,22), (1,23), (1,24))
            i1 = 1 - (i1 ^ s)
            i2 = 1 - (i2 ^ s)
        return (s<<26)|(imm10<<16)|(i1<<13)|(i2<<11)|(imm11)

    def _jump(self, cond, addr):
        # A6.7.12 (T2) B
        if (addr is not None) and (cond == "all"):
            ofs = addr - len(self.code) - 2
            ofs = ofs if ofs >= 0 else (-ofs-1) ^ 0x7FF
            self.code16(0xE000 | (ofs & 0x7ff))
        else:
            # A6.7.12 (T4) B<c> / (T3) B
            uncond = cond == "all"
            ofs = 0 if addr is None else self._make_jump(addr*2, len(self.code) * 2, t3=not uncond)
            base = 0xF0009000 if uncond else (0xF0008000 | (CCODES.index(cond)<<22))
            self.code32(base | ofs)

    def _call(self, addr):
        # If we're withing +/- 16MB use a BL, otherwise load the target addr and BLX
        if abs(addr - (self.base + len(self.code)*2) - 4) < (1<<24):
            ofs = self._make_jump(addr, self.base + len(self.code)*2, t3=False)
            self.code32(0xF000D000|ofs) # A6.7.18 (T1) BL
        else:
            self._imm(RA, addr+1)
            self.code16(0x4780|(RA<<3)) # A6.7.19 - (T1) BLX

    def _imm32(self, base, rd, imm):
        i8, i3, i1, i4 = bit_extract(imm, (8,0), (3,8), (1,11), (4,12))
        self.code32(base|(i1<<26)|(i4<<16)|(i3<<12)|(rd<<8)|(i8))

    def _imm(self, rd, val):
        # A6.7.75 (T1/T3) MOV (imm)
        if abs(val) < 0x100: self.code16(0x2000|(rd<<8)|abs(val))
        elif abs(val) < 0x10000: self._imm32(0xF2400000, rd, abs(val))
        else:
            self._imm32(0xF2400000, rd, val) # A6.7.75 (T3) MOV (imm)
            self._imm32(0xF2C00000, rd, val>>16) #6.7.78 (T1) MOVT
        if val < 0: # negate if negative
            self.code16(0x4240|(rd<<3)|(rd)) # A6.7.106 (T1) RSB (imm)

    def _ldr(self, target, ofs):
        self.code16(0x5800|(target<<3)|(ofs<<6)|target) # A6.7.44 (T1) LDR

    def _str(self, target, base, ofs):
        self.code16(0x5000|(base<<3)|(ofs<<6)|target) # A6.7.120 (T1) STR

    def flush(self):
        for inst in self.insts:
            op, args = inst[0], inst[1:]
            if   op == INS.MOV:   self.code16(0x4600|(args[1]<<3)|args[0])
            elif op == INS.LDSP:  self.code16(0x9800|(args[0]<<8)|(args[1]))
            elif op == INS.STRSP: self.code16(0x9000|(args[0]<<8)|(args[1]))
            elif op == INS.SP:    self.code16((0xB080 if args[0] > 0 else 0xB000)|abs(args[0]))
            elif op == INS.LDR:   self._ldr(args[0], args[1])
            elif op == INS.STR:   self._str(args[0], args[1], args[2])
            elif op == INS.IMM:   self._imm(args[0], args[1])
            elif op == INS.BIN:   self._binop(args[2], args[0], args[1])
            elif op == INS.BINI:  self._binop_imm(args[2], args[0], args[1])
            elif op == INS.JMP:   self._jump(args[0], args[1])
            elif op == INS.CALL:  self._call(args[0])
            elif op == INS.RET:   self.code16(0xBDF8) # A6.7.97 (T1) POP (r3-r7,pc)
            else: raise Exception("unhandled ins: %s" % INS.names[op])
        self.insts = []

    # Intermediate Representation and Optimiser

    def sym_addr(self, funcname):
        if funcname not in self.addrs:
            raise Exception("No native symbol named '%s'" % funcname)
        return self.addrs[funcname]

    def tmp(self):
        self.ntmp += 1
        return self.ntmp

    def inst(self, i, *args):
        # add a IR instruction then try to peephole optimise the last few instructions
        # a couple times. The optimiser is terrible and only looks 2 instructions back
        self.insts.append((i,)+args)
        self.peep()
        self.peep()

    def peep(self):
        if len(self.insts) < 2:
            return
        opa, arga = self.insts[-2][0], self.insts[-2][1:]
        opb, argb = self.insts[-1][0], self.insts[-1][1:]
        repla = None
        if ((opa == INS.IMM) and (opb == INS.MOV) and (arga[0] == argb[1])):
            # [[IMM, x, a], [MOV, b, x]] -> [IMM, b, a]
            repla = (INS.IMM, argb[0], arga[1])
        elif ((opa == INS.MOV) and (opb == INS.MOV) and
              (arga[0] == argb[1]) and (argb[0] == arga[1])):
            # [[MOV, x, y], [MOV, y, x]] -> []
            self.insts.pop()
            self.insts.pop()
        elif ((opa == INS.MOV) and (opb == INS.MOV) and
              (arga[0] == argb[0]) and (arga[1] == argb[1])):
            # [[MOV, x, y], [MOV, x, y]] -> [MOV, x, y]
            self.insts.pop()
        elif ((opa == INS.MOV) and (opb == INS.BIN) and (arga[0] == argb[1])):
            # [[MOV, x, a], [BIN, b, x, c]] -> [BIN, b, a, c]
            repla = (opb, argb[0], arga[1], argb[2])
        #elif ((opa == INS.IMM) and (opb == INS.BIN) and (arga[0] == argb[0]) and
        #      (argb[2] in "add cmp".split())):
            # [[IMM, x, a], [BIN, x, b, c]] -> [BINI, a, b, c]
        #    repla = (INS.BINI, argb[1], arga[1], argb[2])
        elif ((opa == INS.LDSP) and (opb == INS.MOV) and (arga[0] == argb[1])):
            # [[LDSP, x, a], [MOV, b, x]] -> [LDSP, b, a]
            repla = (INS.LDSP, argb[0], arga[1])
        if repla:
            self.insts.pop()
            self.insts[-1] = repla

    def stmp(self, t):
        if t <= 1:  # temps 0 & 1 are stored in registers, else do SP relative store
            self.inst(INS.MOV, t+T0, RA)
        else:
            self.inst(INS.STRSP, RA, self._getvar("#%d" % (t-2)))

    def ltmp(self, t, rd=RB):
        if t <= 1:  # temps 0 & 1 are stored in registers, else do SP relative load
            self.inst(INS.MOV, rd, t+T0)
        else:
            self.inst(INS.LDSP, rd, self._getvar("#%d" % (t-2)))

    def sta(self, n):
        self.inst(INS.MOV, n, RA)

    def _getvar(self, name):
        if name not in self.env:
            self.env[name] = len(self.env)
        return self.env[name]

    def assign(self, target):
        self.inst(INS.STRSP, RA, self._getvar(target))

    def compare(self):
        self.inst(INS.BIN, RA, RB, "cmp")

    def binop(self, op):
        self.inst(INS.BIN, RA, RB, OPS[type(op)])

    def call(self, fn):
        self.inst(INS.CALL, self.sym_addr(fn))
        self.inst(INS.MOV, RA, 0)

    def name(self, name):
        if name in self.env:  # look for args/locals
            varofs = self.env[name]
            self.inst(INS.LDSP, RA, varofs)
        else:  # otherwise assume a global symbol
            self.num(self.sym_addr(name))

    def num(self, value):
        self.inst(INS.IMM, RA, value)

    def mem_load(self):
        self.inst(INS.LDR, RA, RB)

    def mem_store(self, tmp):
        self.ltmp(tmp, rd=RC)
        self.inst(INS.STR, RC, RA, RB)

    def ret(self):
        # we do a bunch of stuff on return, so just jump to a common return point
        self.rets.append(self.jmp())

    def finish(self):
        stk = len(self.env)
        for ret in self.rets:  # patch all our return jumps
            self.patch(ret)
        if stk > 0:  # fix up our stack increment/decrement
            self.code[1] |= stk
            self.inst(INS.SP, -stk)
        self.inst(INS.MOV, 0, RA)  # do the actual return
        self.inst(INS.RET)
        self.flush()

    def label(self):
        self.flush()
        return len(self.code)

    def jmp(self, addr=None, op=None):
        self.inst(INS.JMP, "all" if op is None else OPS[type(op)], addr)
        self.flush()
        return len(self.code)-1

    def patch(self, addr):
        self.flush()  # we're patching an earlier jump, flush all IR so code offsets are correct
        assert (self.code[addr-1] & 0xF000) == 0xF000  # check we're actually modifying the jump
        assert (self.code[addr] in [0x8000, 0x9000])
        ofs = self._make_jump(len(self.code)*2, addr*2 - 2, t3=(self.code[addr] == 0x8000))
        self.code[addr-1] |= (ofs >> 16)
        self.code[addr  ] |= (ofs & 0xFFFF)

# Fronted: cheat, use the python lexer/parser/AST and jam it into our backend in one pass
#
# We linearise nested (binary) ops as follows. left OP right becomes:
# tmp = right; save(tmp)
# RA = left
# RB = load(tmp)
# RA = RA OP RB

def fail(obj):
    raise Exception("Unhandled '%s'" % obj)

def comp_bin_helper(cg, left, right):
    comp_expr(cg, right)
    t = cg.tmp()
    cg.stmp(t)
    comp_expr(cg, left)
    cg.ltmp(t)

def comp_compare(cg, expr, is_test=False):
    if is_test:
        comp_bin_helper(cg, expr.left, expr.comparators[0])
        cg.compare()
    else:  # if the comparison is an expression (foo = x == y), wrap in an if
        comp_expr(cg, ast.IfExp(expr, ast.Num(1), ast.Num(0)))

def comp_binop(cg, expr):
    comp_bin_helper(cg, expr.left, expr.right)
    cg.binop(expr.op)

def comp_call(cg, expr, is_stmt):
    for argnum, arg in enumerate(expr.args):
        comp_expr(cg, arg)
        cg.sta(argnum)
    cg.call(expr.func.id)

def comp_subscript(cg, expr):
    comp_bin_helper(cg, expr.value, expr.slice.value)
    cg.mem_load()

def comp_expr(cg, expr, is_stmt=False, is_test=False):
    if isinstance(expr, ast.Num): cg.num(expr.n)
    elif isinstance(expr, ast.Name): cg.name(expr.id)
    elif isinstance(expr, ast.Compare): comp_compare(cg, expr, is_test)
    elif isinstance(expr, ast.BinOp): comp_binop(cg, expr)
    elif isinstance(expr, ast.Call): comp_call(cg, expr, is_stmt)
    elif isinstance(expr, ast.Subscript): comp_subscript(cg, expr)
    elif isinstance(expr, ast.IfExp):
        comp_if(cg, ast.If(expr.test, [ast.Expr(expr.body)], [ast.Expr(expr.orelse)]))
    else: fail(expr)

def comp_assign(cg, stmt):
    comp_expr(cg, stmt.value)
    if isinstance(stmt.targets[0], ast.Name):  # assign a local/arg
        cg.assign(stmt.targets[0].id)
    else:  # assign a subscript/memory location
        t = cg.tmp()
        cg.stmp(t)
        comp_bin_helper(cg, stmt.targets[0].value, stmt.targets[0].slice.value)
        cg.mem_store(t)

def comp_test(cg, expr):
    if not isinstance(expr, ast.Compare):  # handle non-compare test expressions
        expr = ast.Compare(left=expr, ops=[ast.Eq()], comparators=[ast.Num(0)])
    comp_expr(cg, expr, is_test=True)
    return expr.ops[0]

def comp_while(cg, stmt):
    ltest = cg.label()
    op = comp_test(cg, stmt.test)
    ldone = cg.jmp(op=op)
    comp_block(cg, stmt.body)
    cg.jmp(addr=ltest)
    cg.patch(ldone)

def comp_if(cg, stmt):
    op = comp_test(cg, stmt.test)
    lfalse = cg.jmp(op=REVOP[type(op)]())
    comp_block(cg, stmt.body)
    ldone = cg.jmp()
    cg.patch(lfalse)
    comp_block(cg, stmt.orelse)
    cg.patch(ldone)

def comp_ret(cg, stmt):
    comp_expr(cg, stmt.value) if stmt.value else cg.num(0)
    cg.ret()

def comp_stmt(cg, stmt):
    cg.ntmp = -1
    if isinstance(stmt, ast.Assign): comp_assign(cg, stmt)
    elif isinstance(stmt, ast.While): comp_while(cg, stmt)
    elif isinstance(stmt, ast.If): comp_if(cg, stmt)
    elif isinstance(stmt, ast.Expr): comp_expr(cg, stmt.value, is_stmt=True)
    elif isinstance(stmt, ast.Return): comp_ret(cg, stmt)
    else: fail(stmt)

def comp_block(cg, block):
    for stmt in block:
        comp_stmt(cg, stmt)

def comp_func(cg, func):
    comp_block(cg, func.body)

def comp(src, baseaddr, addrs):
    top = ast.parse(src).body[0]
    is_func = isinstance(top, ast.FunctionDef)
    args = [a.id for a in top.args.args] if is_func else []
    cg = Codegen(baseaddr, addrs, args)
    if is_func:  # if a function, record our address so we can compile recursion
        addrs[top.name] = baseaddr
        comp_func(cg, top)
    else:
        comp_stmt(cg, top)
    cg.finish()
    return array.array("H", cg.code), is_func

# REPL, disassembler, misc bits n bobs

class State(object):
    def __init__(self, baseaddr, addrs):
        self.baseaddr, self.addrs = baseaddr, addrs

def disasm(baseaddr, code):
    code.tofile(open("out.bin", "wb"))
    cmd = ("arm-none-eabi-objdump -D -b binary -marm -Mforce-thumb --adjust-vma=0x%x out.bin" %
           baseaddr).split()
    print "| "+"\n| ".join(subprocess.check_output(cmd).split("\n")[7:-1])

def rep(conn, state):
    first, raw_src = True, ""
    try:  # parse input, handle multiline stuff (poorly)
        while 1:
            raw = raw_input("> " if raw_src == "" else "..")
            raw_src += raw+"\n"
            if (first and icode.compile_command(raw_src)) or ((not first) and raw == ""):
                break
            first = False
    except SyntaxError as exc:
        print "Syntax error:", exc
        return
    # compile code from the prompt into a function, and load into RAM
    code, is_func = comp(raw_src, state.baseaddr, state.addrs)
    disasm(state.baseaddr, code)
    if (len(code) % 2) == 1:  # word align
        code.append(0)
    for i in range(len(code)/2):
        word = (code[i*2+1] << 16) | (code[i*2])
        conn.mem_write(state.baseaddr+i*4, word)
    if is_func:  # if we just defined a function bump the base address
        state.baseaddr += len(code)*2
    else:  # otherwise jump to the code we just generated and print the result
        ret = conn.mem_exec(state.baseaddr+1)  # set Thumb bit for branch
        print struct.unpack("i", struct.pack("I", ret))[0]

def repl(state):
    while 1:
        rep(None, state)

def load_addrs(elf):
    nmout = subprocess.check_output(["arm-none-eabi-nm", elf])
    addrs = {}
    for line in nmout.split("\n"):
        if len(line.split()) == 3:
            addr, _, name = line.split()
            addrs[name] = int(addr, 16)
    return addrs

def main():
    # Base pointer for our code
    state = State(0x2000f000, load_addrs(sys.argv[1]))
    repl(state)

main()
